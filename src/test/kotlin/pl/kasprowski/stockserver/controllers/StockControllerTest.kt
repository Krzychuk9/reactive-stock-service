package pl.kasprowski.stockserver.controllers

import org.junit.jupiter.api.Test
import pl.kasprowski.stockserver.services.PriceService
import reactor.test.StepVerifier
import java.time.Duration

internal class StockControllerTest {

    @Test
    internal fun shouldProduceStockData() {
        val pricesService = PriceService()
        val stockController = StockController(pricesService)

        StepVerifier
                .withVirtualTime { stockController.prices("ANY") }
                .expectSubscription()
                .expectNoEvent(Duration.ofSeconds(1))
                .expectNextMatches { it.symbol == "ANY" && it.price > 0 }
                .thenAwait(Duration.ofSeconds(1))
                .expectNextMatches { it.symbol == "ANY" }
                .thenCancel()
                .verify()
    }
}
